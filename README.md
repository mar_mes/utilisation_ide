TD1 : Gestion des comptes bancaires
Le but de ce projet est de simuler les comptes (courant et épargne) bancaires d’un client en lui
permettant d’effectuer des retraits et des versements.
1. Mettre en place une planification du projet sur GitLab.
2. Définir les classes suivantes avec leurs attributs associés. Pour chaque classe, déclarer les
constructeurs par défaut et surchargés ainsi que les getters/setters.
Client : code (entier), nom (chaine), email (chaine), codeCompte (entier).
Compte : codeCompte (entier), dateCreation (date), solde (double), codeClient (entier).
CompteCourant qui hérite de Compte : decouvert (double).
CompteEpargne qui hérite de Compte : taux (double).
3. Définir une classe Operation avec les attributs :
- clients : une liste de client.
- comptes : une liste de compte.
- localDate : une date.
Cette classe comportera les fonctions suivantes :
- void initClientCompte() : permet d’initialiser une liste de client et une liste de compte.
- void insererClient() : à partir d’un fichier (client.txt) permet d’insérer des clients dans notre
liste.
- void insererCompte() : à partir d’un fichier (compte.txt) permet d’insérer des comptes dans
notre liste.
- Client consulterClient(String nomClient) : permet de vérifier (avec le nom) qu’un client existe
bien dans notre liste.
- void creatClient(Client newClient) : demande les informations d’un nouveau client et l’ajoute
à notre liste de clients. On crée également un nouveau compte à ce nouveau client en
appelant la fonction creatCompte. On l’enregistre ensuite dans notre fichier (client.txt).
- void creatCompte(Compte cpte) : ajoute un compte à notre liste de comptes et dans le
fichier (compte.txt).
- Compte consulterCompte(String nomClient) : permet de vérifier que le client existe bien en
appelant la méthode consulterClient, puis pour ce client qu’un compte existe bien (avec le
code compte).
- String retirer(String nomClient, double montant) : vérifie d’abord que le client a un compte.
Vérifie que le solde et le découvert (compteCourant) autorisé son supérieur au montant
demandé et met ainsi à jour le nouveau solde après le retrait.
- String verser(String nomClient, double montant) : permet de faire un versement sur notre
compte.
4. Créer une classe Principale qui contient la fonction main.
- Instancier un objet Operation et initialiser des clients et des comptes.
- Afficher un message de Bienvenue et demander de s’authentifier avec le nom pour se
connecter sur son compte.
- Une fois connecté, afficher Bonjour Mr/Mme …, Compte n° … et afficher aussi un menu
demandant de sélectionner :
➔ 1 : consulter le compte : afficher les informations sur le compte (n° compte, date création,
solde et n° client).
➔ 2 : effectuer un retrait : demander le montant à retirer et afficher ensuite le nouveau solde
disponible.
➔ 3 : effectuer un virement : demander le montant à verser et afficher le nouveau solde
disponible.
A la fin de chaque choix, il faut demander à l’utilisateur s’il veut effectuer une autre opération. Si oui,
revenir au menu précédent, sinon afficher un message : au revoir et à bientôt…
5. Définir tous les tests.
